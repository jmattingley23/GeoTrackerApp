package xyz.jmatt.geotracker.fragments;

import android.animation.ValueAnimator;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import xyz.jmatt.geotracker.R;

public class AddItemNav extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_additem_nav, container, false);

        Button closeButton = (Button)view.findViewById(R.id.additem_nav_close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClose();
            }
        });

        return view;
    }

    private void handleClose() {
        MapFragment callingFragment = (MapFragment)getFragmentManager().findFragmentByTag(getString(R.string.TAG_map));
        callingFragment.animateFabIn();

        final ValueAnimator fragmentDelayTimer = ValueAnimator.ofInt(0, 700);
        fragmentDelayTimer.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            boolean mainStarted = false;
            boolean navStarted = false;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if((int)animation.getAnimatedValue() > 400 && !mainStarted) {
                    mainStarted = true;
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction(); //new transaction
                    fragmentTransaction.setCustomAnimations(R.animator.slide_in_bottom, R.animator.slide_out_bottom);
                    fragmentTransaction.replace(R.id.frame_additem, new Fragment()); //remove from screen
                    fragmentTransaction.commit();
                }
                if((int)animation.getAnimatedValue() > 600 && !navStarted) {
                    navStarted = true;
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.slide_in_top, R.animator.slide_out_top);
                    fragmentTransaction.replace(R.id.frame_additem_nav, new Fragment());
                    fragmentTransaction.commit();
                }
            }
        });
        fragmentDelayTimer.start();
    }
}
