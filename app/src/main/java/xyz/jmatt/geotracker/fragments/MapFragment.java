package xyz.jmatt.geotracker.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;

import xyz.jmatt.geotracker.R;

public class MapFragment extends Fragment {
    private MapView mapView;
    private GoogleMap googleMap;

    private Bundle savedInstanceState;

    private FloatingActionButton addButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView)view.findViewById(R.id.map);
        this.savedInstanceState = savedInstanceState;

        addButton = (FloatingActionButton)view.findViewById(R.id.add_event_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            //get drawables ready
            AnimatedVectorDrawableCompat plusToClose = AnimatedVectorDrawableCompat.create(getActivity(), R.drawable.avd_plus_to_location);
            AnimatedVectorDrawableCompat closeToPlus = AnimatedVectorDrawableCompat.create(getActivity(), R.drawable.avd_location_to_plus);
            //calculate screen size
            WindowManager wm = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);
            int screenHeight = wm.getDefaultDisplay().getHeight();
            //start with menu closed
            boolean isDialogOpen = false;

            @Override
            public void onClick(View v) {
                handleFabClick();
//                isDialogOpen = !isDialogOpen; //toggle button state
//
//                //get button attributes
//                //icon
//                final AnimatedVectorDrawableCompat currentDrawable = isDialogOpen ? plusToClose : closeToPlus;
//                //icon color
//                Integer toForegroundColor = getActivity().getResources().getColor(isDialogOpen ? R.color.black : R.color.white);
//                Integer fromForegroundColor = getActivity().getResources().getColor(!isDialogOpen ? R.color.black : R.color.white);
//                //button color
//                Integer toBackgroundColor = getActivity().getResources().getColor(isDialogOpen ? R.color.gray : R.color.colorAccent);
//                Integer fromBackgroundColor = getActivity().getResources().getColor(!isDialogOpen ? R.color.gray : R.color.colorAccent);
//
//                //animate background color
//                final ValueAnimator backgroundColorAnimator = ValueAnimator.ofInt(fromBackgroundColor, toBackgroundColor);
//                backgroundColorAnimator.setDuration(500);
//                backgroundColorAnimator.setEvaluator(new ArgbEvaluator());
//                backgroundColorAnimator.setInterpolator(new DecelerateInterpolator(2));
//                backgroundColorAnimator.addUpdateListener(new ObjectAnimator.AnimatorUpdateListener() {
//                    @Override
//                    public void onAnimationUpdate(ValueAnimator animation) {
//                        int animatedValue = (int) animation.getAnimatedValue();
//                        addButton.setBackgroundTintList(ColorStateList.valueOf(animatedValue));
//                    }
//                });
//                backgroundColorAnimator.start();
//
//
//                //animate foreground color
//                final ValueAnimator foregroundColorAnimator = ValueAnimator.ofInt(fromForegroundColor, toForegroundColor);
//                foregroundColorAnimator.setDuration(500);
//                foregroundColorAnimator.setEvaluator(new ArgbEvaluator());
//                foregroundColorAnimator.setInterpolator(new DecelerateInterpolator(2));
//                foregroundColorAnimator.addUpdateListener(new ObjectAnimator.AnimatorUpdateListener() {
//                    @Override
//                    public void onAnimationUpdate(ValueAnimator animation) {
//                        int animatedValue = (int) animation.getAnimatedValue();
//                        currentDrawable.mutate().setColorFilter(animatedValue, PorterDuff.Mode.SRC_ATOP);
//                    }
//                });
//                foregroundColorAnimator.start();
//
//                //animate button position
//                //float end = isDialogOpen ? (-.72f * screenHeight) : 0f;
//                //addButton.animate().translationY(end).setDuration(500).start();
//
//                //set icon & animate transition
//                addButton.setImageDrawable(currentDrawable);
//                currentDrawable.start();
//
//                //add or remove the add item fragment
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction(); //new transaction
//                AddItemFragment fragment = (AddItemFragment)getFragmentManager().findFragmentByTag(getString(R.string.TAG_additem)); //try to find existing fragment
//                if(fragment == null) { //fragment not found
//                    //create new fragment and add
//                    fragmentTransaction.setCustomAnimations(R.animator.slide_in_bottom, R.animator.slide_out_bottom);
//                    fragmentTransaction.add(R.id.frame_additem, new AddItemFragment(), getString(R.string.TAG_additem));
//                    fragmentTransaction.setCustomAnimations(R.animator.slide_in_top, R.animator.slide_out_top);
//                    fragmentTransaction.add(R.id.frame_additem_nav, new AddItemNav());
//                } else { //fragment was already on screen
//                    fragmentTransaction.setCustomAnimations(R.animator.slide_in_bottom, R.animator.slide_out_bottom);
//                    fragmentTransaction.replace(R.id.frame_additem, new Fragment()); //remove from screen
//                    fragmentTransaction.setCustomAnimations(R.animator.slide_in_top, R.animator.slide_out_top);
//                    fragmentTransaction.replace(R.id.frame_additem_nav, new Fragment());
//                }
//                fragmentTransaction.commit();
            }
        });

        return view;
    }

    /**
     * Handles a press of the FloatingActionButton
     */
    private void handleFabClick() {
        if(getFragmentManager().findFragmentByTag(getString(R.string.TAG_additem)) == null) {
            animateFabOut();

            final ValueAnimator fragmentDelayTimer = ValueAnimator.ofInt(0, 500);
            fragmentDelayTimer.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                boolean mainStarted = false;
                boolean navStarted = false;
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    if((int)animation.getAnimatedValue() > 350 && !mainStarted) {
                        mainStarted = true;
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction(); //new transaction
                        fragmentTransaction.setCustomAnimations(R.animator.slide_in_bottom, R.animator.slide_out_bottom);
                        fragmentTransaction.add(R.id.frame_additem, new AddItemFragment(), getString(R.string.TAG_additem));
                        fragmentTransaction.commit();
                    }
                    if((int)animation.getAnimatedValue() > 400 && !navStarted) {
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction(); //new transaction
                        fragmentTransaction.setCustomAnimations(R.animator.slide_in_top, R.animator.slide_out_top);
                        fragmentTransaction.add(R.id.frame_additem_nav, new AddItemNav());
                        fragmentTransaction.commit();
                    }
                }
            });
            fragmentDelayTimer.start();
        } else {
            Toast.makeText(getActivity(), "location", Toast.LENGTH_SHORT).show();
            //todo select location
        }
    }

    /**
     * Animated the colors and icon of the add event button into the set location button
     */
    private void animateFabOut() {
        animateFabBackgroundColor(R.color.colorAccent, R.color.gray);
        animateFabIcon(R.color.white, R.color.black, false);
        animateFabPosition(false);
    }

    /**
     * Animates the colors and icon of the set location button into the add event button
     */
    public void animateFabIn() {
        animateFabBackgroundColor(R.color.gray, R.color.colorAccent);
        animateFabIcon(R.color.black, R.color.white, true);
        animateFabPosition(true);
    }

    /**
     * Animates the color of the FloatingActionButton across 500ms
     * @param fromColor the starting background color
     * @param toColor the ending background color
     */
    private void animateFabBackgroundColor(int fromColor, int toColor) {
        final ValueAnimator backgroundColorAnimator = ValueAnimator.ofInt(
                getActivity().getResources().getColor(fromColor),
                getActivity().getResources().getColor(toColor));
        backgroundColorAnimator.setDuration(500);
        backgroundColorAnimator.setEvaluator(new ArgbEvaluator());
        backgroundColorAnimator.setInterpolator(new DecelerateInterpolator(2));
        backgroundColorAnimator.addUpdateListener(new ObjectAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int animatedValue = (int)animation.getAnimatedValue();
                addButton.setBackgroundTintList(ColorStateList.valueOf(animatedValue));
            }
        });
        backgroundColorAnimator.start();
    }

    /**
     * Animates the color and drawable of the FloatingActionButton
     * @param fromColor the starting icon color
     * @param toColor the ending icon color
     * @param isTransitioningDown whether or not the button is transitioning back down to the bottom
     */
    private void animateFabIcon(int fromColor, int toColor, final boolean isTransitioningDown) {
        int animation = isTransitioningDown ? R.drawable.avd_location_to_plus : R.drawable.avd_plus_to_location;
        final AnimatedVectorDrawableCompat iconDrawable = AnimatedVectorDrawableCompat.create(getActivity(), animation);

        final ValueAnimator foregroundColorAnimator = ValueAnimator.ofInt(
                getActivity().getResources().getColor(fromColor),
                getActivity().getResources().getColor(toColor));
        foregroundColorAnimator.setDuration(500);
        foregroundColorAnimator.setEvaluator(new ArgbEvaluator());
        foregroundColorAnimator.setInterpolator(new DecelerateInterpolator(2));
        foregroundColorAnimator.addUpdateListener(new ObjectAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int animatedValue = (int)animation.getAnimatedValue();
                iconDrawable.mutate().setColorFilter(animatedValue, PorterDuff.Mode.SRC_ATOP);
            }
        });
        foregroundColorAnimator.start();

        addButton.setImageDrawable(iconDrawable);
        iconDrawable.start();
    }

    /**
     * Animates the position of the FloatingActionButton on screen
     * @param isTransitioningDown whether or not the button is moving back to the bottom of the screen
     */
    private void animateFabPosition(final boolean isTransitioningDown) {
        final ValueAnimator positionDelayTimer = ValueAnimator.ofInt(0, 500);
        positionDelayTimer.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            boolean started = false;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if((int)animation.getAnimatedValue() > 250 && !started) {
                    started = true;
                    if(isTransitioningDown) {
                        addButton.animate().translationY(0f).setDuration(500).start();
                    } else {
                        addButton.animate().y(getResources().getDimensionPixelSize(R.dimen.additem_fab_height)).setDuration(500).start();
                    }
                }
            }
        });
        positionDelayTimer.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                googleMap = map;
            }
        });
    }
}
