package xyz.jmatt.geotracker.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import xyz.jmatt.geotracker.fragments.MapFragment;
import xyz.jmatt.geotracker.models.UserSingleton;
import xyz.jmatt.geotracker.R;

public class MapActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_map);
//        TextView welcome = (TextView) findViewById(R.id.welcome_text);
//        welcome.setText("Welcome, " + UserSingleton.getInstance().getUserProfile().getDisplayName() + "!");
//
//        Button signOutButton = (Button)findViewById(R.id.google_sign_out);
//        signOutButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//                return;
//            }
//        });

        MapFragment fragment = new MapFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment, getString(R.string.TAG_map));
        fragmentTransaction.commit();

        setContentView(R.layout.activity_map);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        if(bar != null) {
            bar.setHomeButtonEnabled(false);
            bar.setDisplayHomeAsUpEnabled(false);
            bar.setDisplayShowHomeEnabled(false);
        }
        return true;
    }
}
