package xyz.jmatt.geotracker.models;

import com.google.firebase.auth.FirebaseUser;

/**
 * Holds information about the current User
 */
public class UserSingleton {
    private static final UserSingleton INSTANCE = new UserSingleton();
    private static FirebaseUser userProfile;

    private UserSingleton() {}

    public static UserSingleton getInstance() {
        return INSTANCE;
    }

    public void setUserProfile(FirebaseUser user) {
        userProfile = user;
    }

    public FirebaseUser getUserProfile() {
        return userProfile;
    }
}
